<?php

return [
    'Prologue' => [
        'url' => 'docs/prologue',
        'children' => [
            'Release note' => 'docs/release-notes',
            'Upgrade guide' => 'docs/upgrade-guide',
            'Contribution guide' => 'docs/contribution-guide',
            'API documentation' => 'docs/api-documentation',
        ],
    ],
    'Getting Started' => [
        'url' => 'docs/getting-started',
        'children' => [
            'What is OfficeLife' => 'docs/getting-started',
            'Values' => 'docs/values',
            'Open startup' => 'docs/open-startup',
            'Development' => 'docs/development',
        ],
    ],
    'Teams' => [
        'url' => 'docs/teams',
        'children' => [
            'What are teams' => 'docs/what-are-teams',
            'Team description' => 'docs/values',
            'Open startup' => 'docs/open-startup',
            'Development' => 'docs/development',
        ],
    ],
    'Official site' => 'https://officelife.io',
];
