<?php

return [
    'baseUrl' => '',
    'production' => false,
    'siteName' => 'OfficeLife Documentation Portal',
    'siteDescription' => 'Everything you need to know about OfficeLife',

    // Algolia DocSearch credentials
    'docsearchApiKey' => '',
    'docsearchIndexName' => '',

    // navigation menu
    'navigation' => require_once('navigation.php'),

    // helpers
    'isActive' => function ($page, $path) {
        return ends_with(trimPath($page->getPath()), trimPath($path));
    },
    'isActiveParent' => function ($page, $menuItem) {
        if (is_object($menuItem) && $menuItem->children) {
            return $menuItem->children->contains(function ($child) use ($page) {
                return trimPath($page->getPath()) == trimPath($child);
            });
        }
    },
    'url' => function ($page, $path) {
        return starts_with($path, 'http') ? $path : '/' . trimPath($path);
    },
    'lastModificationDate' => function ($page) {
        return \Carbon\Carbon::createFromTimestamp($page->getModifiedTime())->toCookieString();
    },
];
