---
title: Values
description: All the values OfficeLife lives by.
extends: _layouts.documentation
section: content
---

# Values {#values}

OfficeLife is a one-man company. My project, my baby, in fact. In everything that I do, I try to follow the following values.

* Color is not important.