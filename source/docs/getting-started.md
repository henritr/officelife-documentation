---
title: Getting Started
description: Getting started with OfficeLife is as easy as 1, 2, 3.
extends: _layouts.documentation
section: content
---

# Getting Started {#getting-started}

We live in an age where there is a tool for every specific aspect of a company: HR, project management, team management, 1 on 1 management… If a company wants to have a complete 360 view on what’s happening inside its walls, it needs to buy and configure a lot of tools.

OfficeLife exists to offer a solution for this problem. I think most companies need a single tool that addresses 70% of the job, instead of having 30 different specific tools that do 100%.

At any given point, managers and employees want to know:

* How do the employees feel,
* What is the current structure of the teams, offices, who manages who,…
* What people are working on right now,
* How can we improve the communication between employees and teams.

I believe that all those answers come from knowing employees really well. This is why it all starts with a basic Human Resources toolset.

Good luck finding a tool that answers all those points. Some tools do this, but they are also hard to use, hard to configure and not user friendly.

This is why I’ve built OfficeLife. Its main goals are to:

* give an overview of how employees and managers feel right now,
* help managers be more empathetic,
* help companies be better at managing people's dreams and ambitions,
* help employees better communicate with their employers.

OfficeLife aims to be a combination of softwares like BambooHR, Officevibe and Monica – although in a single application and at a much more reasonable price.

Below is a comprehensive list of all the features of OfficeLife:

* Ability to manage all employees at a Human Resources level,
* Ability to manage teams
    * Associate employees to them,
    * Broadcast news about the team to all team members,
* Ability to broadcast company news,
* Ability to define holidays and time offs schedules on a yearly basis,
* From an employee perspective:
    * Ability to define name, email and physical address
    * Ability to define managers and direct reports (link between employees),
    * Ability to add daily work logs to inform the team and the company,
    * Ability to indicate how you feel on any given day,
    * Ability

## Why is OfficeLife different {#different}

First and foremost, OfficeLife is different because it doesn’t try to cover all the use cases for every problem. I don't try to suit everyone. OfficeLife tries very hard to be really simple to use and bring a set of features that most companies will find useful. OfficeLife is strongly opinionated.

It also gives users and companies a complete control over their data. You are not locked in the product, nor will you ever be. All the data, both from a company and a user points of view, can be exported for free at any time (even if you don't have a paid account anymore), in a standard JSON or mySQL format. You can also use the API without any restrictions.

Privacy is more than paramount to me – it’s essential. As a matter of fact, I don't track users and I don't use ads or sell data. We might be the only tool of this kind to have this kind of behaviour.

Technically, the software is developed with boring, proven, predictable, easy to maintain technologies that make the tool fast and secure. I want to create a product useful for users and companies, not something that is technologically exciting.

In terms of user experience, the software aims to be simple to use with the minimum amount of configuration. The design itself is not a priority - user experience is.

Finally, OfficeLife is open source and can be installed on your own server if you so desire, for free and without any fees.

## Pricing and open source {#pricing}

OfficeLife comes in two flavours: open source and hosted.

The open source version is the one you will find on GitHub. The code is released with the MIT license, which means you can do whatever you want with it. You can download the tool for free, whenever you want, and have a complete control over everything. You don't need a product key for this. However, you are responsible for your own hosting, maintenance and upgrade of your instance.

The hosted version, which can be found on https://officelife.io, is the exact same version as the open source one. As a matter of fact, the code that I deploy in production comes from the repository hosted on Github. I take care of maintaining this instance, of upgrading it and making backups every day of your precious data.

While the open source version is free, the hosted version is supported by subscriptions. You pay a fair amount of money each month, and in exchange you have access to your own OfficeLife’s account. The price is fixed, $100 USD per month (or $1000 USD per year — a good discount!), no matter the number of employees, users or company size. Yes, I hate those other services which charge a small amount of money per user, like $2 per user – you think it’s cheap, but if your company has 200 employees, you end up with $400 USD to pay each month, which is insane.

### Isn’t this price too expensive for my tiny company?

Perhaps. And I’m sorry about this. However if the price is too expensive for you, which I completely understand, you can still download the open source version on Github and install it on your own server. It’s cheaper, and also safer in terms of privacy 😀

#### Why is this not more expensive?

Well. OfficeLife is my baby and I’m the only one working on it. As a matter of fact I work as hard as I can on this product, day and night. I don't have employees to pay. I don't have a fancy office. I just need to cover my family needs. If I can convince 200 companies to pay for this service, I can make a decent living. And it’s enough for me.